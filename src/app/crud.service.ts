import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';



@Injectable({
  providedIn: 'root'
})
export class CrudService {

  private server=environment.serverUrl;
  
  constructor(private http:HttpClient) { }

createModel(path,model) :Observable<any>{
  return this.http.post(`${this.server}${path}`,model);
}

}
