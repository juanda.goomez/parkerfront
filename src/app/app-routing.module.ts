import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParkerComponent } from './components/parker/parker.component';

const routes: Routes = [
  {path:'',component:ParkerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
