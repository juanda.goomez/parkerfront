import { CrudService } from './../../crud.service';
import { VehicleModel } from './../../vehicle-model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parker',
  templateUrl: './parker.component.html',
  styleUrls: ['./parker.component.css']
})
export class ParkerComponent implements OnInit {
  modelVehicle:VehicleModel

  constructor(private serviceCrud:CrudService) { }

  ngOnInit() {
    this.modelVehicle=new VehicleModel();
  }

  sendModel(){
    const path='vehicle/create'
    this.serviceCrud.createModel(path,this.modelVehicle).subscribe(
      result=>{
          console.log(result);
      },
      eror=>{
         console.log("error");
      }
    )
    
  }

}
