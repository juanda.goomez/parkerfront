export class VehicleModel{
    color:string;
    plate:string;
    model:string;

    constructor(){
        this.color='';
        this.model='';
        this.plate='';
    }
}